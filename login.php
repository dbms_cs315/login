
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Online-Banking</title>
	<meta charset="utf-8" />
	
	<meta name="viewport" content="width-devic, initial-scale=1.0">
	<link rel = "stylesheet" href="css/login.css" type="text/css">
</head>

<body class="body">
	<!--Header begin here-->

	<div class="logo">
		<img src="images/logo.jpg">
	</div>
	
	<header class="mainheader">
		<nav><ul>
			<li><a href="index1.php">Home</a></li>
			<li><a href="#" title="FAQ">Product & Service</a></li>
			<li><a href="#">How Do I</a></li>
		</ul></nav>
	</header>

	<div class="wellcome">
		<p>Kindly change your login password if it is older than a month.<br>It is mandatory to change login passowrd in a month.</p>
	</div>	

	<div class="login-info">
		<h4>Login to Online-Banking</h4><p>Welcome to Personal Internet Banking</p>
	</div>

	<div class="login">
		<div class="se"><p>(<span class="security-det">CARE</span>: Username and password are case sensitive.)</p></div>

	<form action="#" method="post" enctype="multipart/form-data" id="acclogin">
		<div class="errorMessage" align="center"><?php echo $errorMessage; ?></div> 		  
	         
	<div class=acc>	
		<p>Account No:</p>            	
        
		<span id="sprytextfield1" style="text-align:left;">
                	<input name="accno" type="text" id="accno" tabindex="10" size="30" maxlength="30" />
            	    	 <span class="textfieldRequiredMsg">Account Number is required.</span>
			<span class="textfieldInvalidFormatMsg">Invalid Account Number.</span>
		</span> 
             
		<p>Password:</p>
			<span id="sprypassword1" style="text-align:left;"> 
		     		<input name="pass" type="password" id="pass" tabindex="20" size="30" />
		      		<span class="passwordRequiredMsg">Password is required.</span>
				<span class="passwordMinCharsMsg">You must specify at least 6 characters.</span>
				<span class="passwordMaxCharsMsg">You must specify at max 10 characters.</span>
			</span>
			   
		 <input name="submitButton" type="submit" id="submitButton" value="Login Now ! " />
		 
		<p>If your account is not register with us, please <a href="<?php echo WEB_ROOT; ?>register.php">Register</a>.              

	<!-- <script>
		var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "integer", {validateOn:["blur", "change"]});
		var sprypassword1 = new Spry.Widget.ValidationPassword("sprypassword1", {minChars:6, maxChars: 12, validateOn:["blur", "change"]});
	</script> -->
	</div>
	</form>
		<img src="images/login_2.jpg">
	</div>

	<div class="sec-info1">
		<p><span class="security-det">NEVER</span> respond to any popup, email, SMS or phone call, no matter how appealing or official looking, seeking your personal information such as username, password(s), mobile number, ATM Card details, etc. Such communications are sent or created by fraudsters to trick you into parting with your credentials.</p>
	</div>

	<div class="sec-info2">
		<p><img src="images/arrow.gif">  Mandatory fields are marked with an asterisk (*)</p>
		<p><img src="images/arrow.gif">  Do not provide your username and password anywhere other than in this page</p>
		<p><img src="images/arrow.gif">  Your username and password are highly confidential. Never part with them.SBI will never ask for this information.</p>
	</div>

<div class="sec-info3">
<p>This site is certified by Verisign as a secure and trusted site. All information sent or received in this site is encrypted.</p>
</div>

<!-- Footer begin here-->
<footer class="mainfooter">
		<p>Copyright &copy; <a href="#" title="test">Test.com</a></p>
	</footer>
<!--Footer end here-->

</body>
</html>
