<!DOCTYPE html>
<html lang="en">

<head>
	<title>Online-Banking</title>
	<meta charset="utf-8" />
	
	<link rel = "stylesheet" href="css/style.css" type="text/css">
	<meta name="viewport" content="width-devic, initial-scale=1.0">
</head>

<body class="body">

	<!--Header begin here-->

	<div class="logo">
		<img src="images/logo.jpg">
	</div>
	<header class="mainheader">
		<nav><ul>
			<li><a href="javascript:void(0);">Service</a>
				<ul>
					<li><a href="login.php">Personal Internet Banking</a></li>
					<li><a href="#">Corporate Internet Banking</a></li>
				</ul></li>
			<li><a href="#" title="FAQ">FAQ</a></li>
			<li><a href="register.php">Apply for Account</a></li>
			<li><a href="#">Contact Us</a></li>
			<li><a href="#">About Us</a></li>
		</ul>
		
			<div class="social-net">
			   <!-- <img src="images/youtube.png">
			   <img src="images/twitter.jpg"> -->			  
			 <img src="images/fb.png">
			</div>
		</nav>	
	</header>
<!--Header end here-->

	<!-- wellcome begin here -->
	<div class="wellcome">
		<p>Welcome to Online-Banking<br>If your page is hazy, Please refresh it.</p>
	</div>
	<!-- Wellcome end here -->

	<!-- Feature begin here -->
	<div class="content">
	<article>
		<p><img src="images/arrow.gif"> Manage your account conveniently through the internet.</p>
		<p><img src="images/arrow.gif"> Full details of your account till date.</p>
		<p><img src="images/arrow.gif"> Access anything, from anywhere</p>
		<p><img src="images/arrow.gif"> It's heighly secure.</p>
	</article>
	<!-- feature end here -->
	</div>
	<!-- Login-botton begin here -->
	<side class="login-cont">
		<h3>Click here to login</h3>
		<a href="login.php"><img src="images/login_1.jpg"></a>
	</side>
	<!-- Login-botton end here -->
	
	<!-- Image-slide begin here -->
<div id="captioned-gallery">
	<figure class="slider">
		<figure>
			<img src="images/slide1.png" alt>
		</figure>
		<figure>
			<img src="images/slide2.jpg" alt>
		</figure>
		<figure>
			<img src="images/slide3.jpg" alt>
		</figure>
		<figure>
			<img src="images/login1.jpg" alt>
		</figure>
		<figure>
			<img src="images/bank.jpg" alt>
		</figure>

	</figure>
</div>
	<!-- image-slide end here -->

<!-- Footer begin here-->
<footer class="mainfooter">
		<p>Copyright &copy; <a href="#" title="test">Test.com</a></p>
	</footer>
<!--Footer end here-->
</body>
</html>

